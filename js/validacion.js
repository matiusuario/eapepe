$(document).ready(function(){
	$("#postulante").validate({
		rules: {
			nombre: {
				required: true,
				minlength: 2
			},
			apellido: {
				required: true,
				minlength: 2
			},
			sexo: "required",
			dni: {
				required: true,
				digits: true,
				range: [1000000, 99999999]
			},
			telefono: {
				digits: true,
				range: [1000000000, 9999999999999]
			},
			email: {
				required: true,
				email: true
			},
			provincia: "required",
			ciudad: {
				required: true,
				minlength: 3
			},
			puesto: "required",
			consentimiento: "required"
		},
		messages: {
			nombre: "Por favor complete este campo",
			apellido: "Por favor complete este campo",
			sexo: "Por favor maque una opción",
			dni: "Por favor complete este campo",
			telefono: "Por favor complete este campo",
			email: "Por favor ingrese un email válido",
			provincia: "Por favor seleccione una provincia",
			ciudad: "Por favor complete este campo",
			puesto: "Por favor seleccione una opción",
			consentimiento: "Por favor marque esta casilla para continuar"
		},
	});
});
