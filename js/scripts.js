$(document).ready(function(){
	$("#enviar").hide();
	$("#mostrarResumen").click(function(){
		$("#nom").text("Nombre: " + $("#nombre").val());
		$("#ape").text("Apellido: " + $("#apellido").val());
		$("#sex").text("Sexo: " + $("[name='sexo']:checked").val());
		$("#doc").text("DNI: " + $("#dni").val());
		$("#tel").text("Teléfono: " + $("#telefono").val());
		$("#mail").text("E-Mail: " + $("#email").val());
		$("#ciud").text("Ciudad: " + $("#ciudad").val());
		$("#prov").text("Provincia: " + $("#provincia").val());
		$("#puest").text("Cargo al que aspira: " + $("#puesto").val());
	});
	$("#continuar").click(function(){
		$("#enviar").show();
	});
});
